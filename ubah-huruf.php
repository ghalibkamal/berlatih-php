<?php
    function ubah_huruf($str) {
        $len = strlen($str);

        for ($i = 0; $i < $len; ++$i) {
            if ($str[$i]) {
                if ($str[$i] == 'z') {
                    $str[$i] = 'a';
                }
                else {
                    $str[$i] = chr(ord($str[$i]) + 1);
                }
            }
        }

        echo $str . "<br>";
    }
    
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>