<?php
function tukar_besar_kecil($string){
    
    $str = '';
        $length = strlen($string);
        for ($i=0 ; $i<$length ; $i++) {
            if ($string[$i] >= 'A' && $string[$i] <= 'Z') {
                $str .= strtolower($string[$i]);
            } else if ($string[$i] >= 'a' && $string[$i] <= 'z') {
                $str .= strtoupper($string[$i]);
            } else {
                $str .= $string[$i];
            }
        }
        echo $str . "<br>";

}



// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>